" General
set nocompatible " Ignore vi compatibility
set modelines=0 " Prevents some security exploits

set nobackup
set nowritebackup
set noswapfile

set vb t_vb= " Stop beping and flashing

set hidden " Enable unsaved buffers

set wildmenu " Menu above the command line with completion
set wildmode=list:longest,full " List all matches + complete longest match + wildmenu

set autochdir " Use the directory of file as working directory

" Remap F1 to Esc
imap <f1> <esc>
nmap <f1> <esc>
vmap <f1> <esc>

" ; does the same as :
nmap ; :

" Appearance
set t_Co=256
set background=light
colorscheme dl_macvim
"set langmenu=en
"language messages en

set nu " Turn on line numbers
"set relativenumber " Relative numbering

if has("gui_macvim")
  set cursorline
endif

set ruler
set laststatus=2 " Always show the status line
set showmode " Show current vim mode
set showcmd " Show current vim command in status bar

set showmatch " Show matching parentheses
" Tab key match bracket pairs in normal and visual modes
"nmap <tab> %
"vmap <tab> %

set scrolloff=3 " Set number of lines below or above the cursor while scrolling

" Editing
set bs=2 " Allow backspace to delete

syntax on
filetype on
filetype indent on
filetype plugin on
au BufRead,BufNewFile *.cf set filetype=bnfc
au! Syntax bnfc source ~/.vim/syntax/bnfc.vim

set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

set autoindent

set wrap linebreak " Set wrap text without breaking words
set showbreak=... " Display ... at the beginning of wrapped line

" Move by the displayed line
nmap j gj
nmap k gk
vmap j gj
vmap k gk

" jj in insert mode behaves like Esc
imap jj <esc>

" Learn hjkl at last (disables arrow keys)
"nmap <up> <nop>
"nmap <down> <nop>
"nmap <left> <nop>
"nmap <right> <nop>
"imap <up> <nop>
"imap <down> <nop>
"imap <left> <nop>
"imap <right> <nop>

" Searching
set ignorecase
set smartcase " Override the ignorecase if the search pattern contains upper case characters

set incsearch
set hlsearch

" Don't jump when highlighting the word
nnoremap * *N

" Use normal regular expressions in normal and visual mode
nmap / /\v
vmap / /\v

" Clear search with \<space>
nmap <leader><space> :noh<cr>

" Arrow keys new functionality
function! ExchangeWithLineAbove()
  if line(".") == 1
    return
  endif
  let l:curlinenum = line(".")
  let l:curline = getline(l:curlinenum)
  let l:abovelinenum = line(".") - 1
  let l:aboveline = getline(l:abovelinenum)
  let l:col = col(".")
  call setline(l:abovelinenum, l:curline)
  call setline(l:curlinenum, l:aboveline)
  call cursor(l:abovelinenum, l:col)
endfunction

function! ExchangeWithLineBelow()
  if line(".") == line("$")
    return
  endif
  let l:curlinenum = line(".")
  let l:curline = getline(l:curlinenum)
  let l:belowlinenum = line(".") + 1
  let l:belowline = getline(l:belowlinenum)
  let l:col = col(".")
  call setline(l:belowlinenum, l:curline)
  call setline(l:curlinenum, l:belowline)
  call cursor(l:belowlinenum, l:col)
endfunction

nmap <silent> <C-left> <<
nmap <silent> <C-right> >>
nmap <silent> <C-up> <esc>:call ExchangeWithLineAbove()<cr>
nmap <silent> <C-down> <esc>:call ExchangeWithLineBelow()<cr>

vmap <silent> <C-left> <
vmap <silent> <C-right> >
vmap <silent> <C-up> <esc>:call ExchangeWithLineAbove()<cr>
vmap <silent> <C-down> <esc>:call ExchangeWithLineBelow()<cr>

imap <silent> <C-left> <C-D>
imap <silent> <C-right> <C-T>
imap <silent> <C-up> <esc>:call ExchangeWithLineAbove()<cr>
imap <silent> <C-down> <esc>:call ExchangeWithLineBelow()<cr>

"Use TAB to complete when typing words, else inserts TABs as usual.
"Uses dictionary and source files to find matching words to complete.
function! Tab_Or_Complete()
  if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
    return "\<C-N>"
  else
    return "\<Tab>"
  endif
endfunction
inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>
set dictionary="/usr/share/dict/words"

" Use Option(Alt)-Tab for Omni completion
inoremap <M-Tab> <C-X><C-O>

" ConqueTerm
let g:ConqueTerm_FastMode = 1
let g:ConqueTerm_ReadUnfocused = 1
let g:ConqueTerm_InsertOnEnter = 1
let g:ConqueTerm_CloseOnEnd = 1

" Next and previous window (Tab and Shift-tab)
nnoremap <Tab> <C-W>w
nnoremap <S-Tab> <C-W>W

" netrw (file explorer)
let g:netrw_liststyle=3    " Use tree-mode as default view
let g:netrw_winsize=20     " 20% of window for file explorer
let g:netrw_browse_split=4 " Open file in previous buffer

let g:airline_theme='bubblegum'

