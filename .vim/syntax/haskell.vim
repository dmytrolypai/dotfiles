" Vim syntax file
" Language: Haskell
" Latest Revision: 18 December 2012

if exists("b:current_syntax")
  finish
endif

syn keyword hsKeywords as case of class data family default deriving do forall foreign hiding if then else import infix infixl infixr instance let in module newtype proc qualified rec type where

" Order matters
syn match hsVarSym "\(\<[A-Z][a-zA-Z0-9_']*\.\)\=[-!#$%&\*\+/<=>\?@\\^|~.][-!#$%&\*\+/<=>\?@\\^|~:.]*"
syn match hsConSym "\(\<[A-Z][a-zA-Z0-9_']*\.\)\=:[-!#$%&\*\+./<=>\?@\\^|~:]*"
syn match hsVarSym "`\(\<[A-Z][a-zA-Z0-9_']*\.\)\=[a-z][a-zA-Z0-9_']*`"
syn match hsConSym "`\(\<[A-Z][a-zA-Z0-9_']*\.\)\=[A-Z][a-zA-Z0-9_']*`"

syn match hsType "\(\<[A-Z][a-zA-Z0-9_']*\.\)\=\<[A-Z][a-zA-Z0-9_']*\>"

syn match  hsLineComment  "---*\([^-!#$%&\*\+./<=>\?@\\^|~].*\)\?$" contains=hsTodo
syn region hsBlockComment start="{-" end="-}" contains=hsBlockComment,hsTodo
syn region hsPragma       start="{-#" end="#-}"
syn keyword hsTodo contained TODO

syn region hsString		 start=+"+  skip=+\\\\\|\\"+  end=+"+
syn match  hsCharacter "[^a-zA-Z0-9_']'\([^\\]\|\\[^']\+\|\\'\)'"lc=1
syn match  hsCharacter "^'\([^\\]\|\\[^']\+\|\\'\)'"
syn match  hsNumber		 "\<[0-9]\+\>\|\<0[xX][0-9a-fA-F]\+\>\|\<0[oO][0-7]\+\>"
syn match  hsFloat		 "\<[0-9]\+\.[0-9]\+\([eE][-+]\=[0-9]\+\)\=\>"
syn match  hsEmptyList "\[\]"

sy match hsTHIDTopLevel   "^[a-z]\S*" 
sy match hsTHTopLevel     "^\$(\?" nextgroup=hsTHTopLevelName 
sy match hsTHTopLevelName "[a-z]\S*" contained

hi def link hsKeywords     Statement

hi def link hsVarSym       hsOperator
hi def link hsConSym       hsOperator
hi def link hsOperator     Operator

hi def link hsType         Type

hi def link hsLineComment  hsComment
hi def link hsBlockComment hsComment
hi def link hsComment      Comment
hi def link hsPragma       SpecialComment
hi def link hsTodo         Todo

hi def link hsString       String
hi def link hsCharacter    String
hi def link hsNumber       Constant
hi def link hsFloat        Constant
hi def link hsEmptyList    Constant

hi def link hsTHIDTopLevel   Macro
hi def link hsTHTopLevelName Macro

let b:current_syntax = "haskell"
