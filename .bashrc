export PATH="$HOME/Library/Haskell/bin:$PATH"
alias mvim='open -a "/Applications/MacVim.app"'
alias ls='ls -ohp'
alias am='open -a "Activity Monitor"'
alias fsi='fsharpi --readline-'

source ~/.git-completion.bash

export PS1='\[$(tput setaf 2)\]\W\[$(tput setaf 1)\]`__git_ps1 " (%s)"` \[$(tput setaf 7)\]$ '
# " quotes break the __git_ps1
# variables for colors break the command line completion for long lines

export PIPATH="$HOME/Work/paragon/lib"

[[ -s `brew --prefix`/etc/autojump.sh ]] && . `brew --prefix`/etc/autojump.sh

