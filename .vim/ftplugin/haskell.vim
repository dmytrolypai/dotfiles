" Remap F2 to load file with ghci
nmap <f2> :w<cr>:ConqueTermVSplit ghci <c-r>%<cr>
vmap <f2> :w<cr>:ConqueTermVSplit ghci <c-r>%<cr>

" neco-ghc
setlocal omnifunc=necoghc#omnifunc

" hdevtools
nnoremap <buffer> <F3> :GhcModType<CR>
nnoremap <buffer> <silent> <F4> :GhcModTypeClear<CR>
nnoremap <buffer> <F5> :GhcModCheck<CR>
nnoremap <buffer> <F6> :GhcModLint<CR>
nnoremap <buffer> <F7> :GhcModInfo<CR>

