" Vim syntax file
" Language: BNFC
" Maintainer: Dmytro Lypai <dmytrolypai@gmail.com>
" Latest Revision: 29 January 2013

if exists("b:current_syntax")
  finish
endif

syn keyword bnfcKeywords separator terminator nonempty coercions comment entrypoints token
syn match bnfcProduction "::="
syn match bnfcDot "\."

syn match bnfcLineComment "--.*$"
syn region bnfcBlockComment start="{-" end="-}" contains=bnfcBlockComment

syn region bnfcTerminal start=+"+ end=+"+

syn match  bnfcCharacter "[^a-zA-Z0-9_']'\([^\\]\|\\[^']\+\|\\'\)'"lc=1
syn match  bnfcCharacter "^'\([^\\]\|\\[^']\+\|\\'\)'"

hi def link bnfcKeywords     Statement
hi def link bnfcProduction   bnfcOperator
hi def link bnfcDot          bnfcOperator
hi def link bnfcOperator     Operator
hi def link bnfcLineComment  bnfcComment
hi def link bnfcBlockComment bnfcComment
hi def link bnfcComment      Comment
hi def link bnfcTerminal     String
hi def link bnfcCharacter    Character

let b:current_syntax = "bnfc"
