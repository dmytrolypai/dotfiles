" Modification of MacVim colorscheme. A lot of colours are borrowed from
" Solarized.
"
" Maintainer:   Dmytro Lypai <dmytrolypai@gmail.com>
" Last Change:  2014 April 07
"

highlight clear

" Reset String -> Constant links etc if they were reset
if exists("syntax_on")
  syntax reset
endif

runtime colors/macvim.vim

let g:colors_name = "dl_macvim"

if &background == "light"
  hi Normal guibg=#fdf6e3 guifg=#586e75 ctermbg=230 ctermfg=240

  hi Macro guifg=#0e74b8 ctermfg=25

  hi Constant gui=NONE guifg=#b58900 ctermfg=136
  hi String   gui=NONE guifg=#b58900 ctermfg=136

  hi CursorColumn guibg=#eee8d5 ctermbg=254
  hi CursorLine   guibg=#eee8d5 ctermbg=254

  hi Comment      guifg=#93a1a1 ctermfg=245

  hi Statement      guifg=Maroon ctermfg=124

  hi MatchParen   guifg=Black guibg=#cdc9c9 ctermbg=darkgray ctermfg=white

  hi Search       guibg=#87af00 guifg=#fdf6e3 ctermbg=106 ctermfg=230

  hi LineNr       guifg=#93a1a1 guibg=#eee8d5 ctermfg=245 ctermbg=254
endif
